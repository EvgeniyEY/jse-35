package ru.ermolaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;

@Component
public class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    @EventListener(condition = "(@versionListener.command() == #event.name) || (@versionListener.arg() == #event.name)")
    public void handler(final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println("1.3.3");
    }

}
