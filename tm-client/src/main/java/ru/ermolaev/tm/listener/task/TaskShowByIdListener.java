package ru.ermolaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.TaskDTO;
import ru.ermolaev.tm.endpoint.TaskEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class TaskShowByIdListener extends AbstractTaskListener {

    @Autowired
    public TaskShowByIdListener(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return "task-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @NotNull final TaskDTO task = taskEndpoint.findTaskById(session, id);
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

}
