package ru.ermolaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ermolaev.tm.api.endpoint.IProjectEndpoint;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
@Controller
@NoArgsConstructor
public final class ProjectEndpoint implements IProjectEndpoint {

    private ISessionService sessionService;

    private IProjectService projectService;

    @Autowired
    public ProjectEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IProjectService projectService
    ) {
        this.sessionService = sessionService;
        this.projectService = projectService;
    }

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        sessionService.validate(sessionDTO);
        projectService.createProject(sessionDTO.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        sessionService.validate(sessionDTO);
        projectService.updateById(sessionDTO.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date
    ) throws Exception {
        sessionService.validate(sessionDTO);
        projectService.updateStartDate(sessionDTO.getUserId(), id, date);
    }

    @Override
    @WebMethod
    public void updateProjectCompleteDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date
    ) throws Exception {
        sessionService.validate(sessionDTO);
        projectService.updateCompleteDate(sessionDTO.getUserId(), id, date);
    }

    @NotNull
    @Override
    @WebMethod
    public Long countAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return projectService.countAllProjects();
    }

    @NotNull
    @Override
    @WebMethod
    public Long countUserProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return projectService.countUserProjects(sessionDTO.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return ProjectDTO.toDTO(projectService.findOneById(sessionDTO.getUserId(), id));
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return ProjectDTO.toDTO(projectService.findOneByName(sessionDTO.getUserId(), name));
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return projectService.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return projectService.findAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        sessionService.validate(sessionDTO);
        projectService.removeOneById(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        sessionService.validate(sessionDTO);
        projectService.removeOneByName(sessionDTO.getUserId(), name);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        projectService.removeAll();
    }

    @Override
    @WebMethod
    public void clearProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        projectService.removeAllByUserId(sessionDTO.getUserId());
    }

}
