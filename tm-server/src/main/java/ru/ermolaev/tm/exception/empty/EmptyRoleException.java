package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public final class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty.");
    }

}
