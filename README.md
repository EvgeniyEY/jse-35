# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EVGENIY ERMOLAEV

**E-MAIL**: ermolaev.evgeniy.96@yandex.ru

# SOFTWARE

- JDK 1.8

- Windows 10

# PROGRAM BUILD

```bash
mvn clean install
```

# DOCKER COMMANDS
### RUN CLUSTER
```bash
docker-compose up -d
```
### SHUTDOWN CLUSTER
```bash
docker-compose down
```

# SCREENSHOTS

https://yadi.sk/d/LWb-ciTc3PY0Sw?w=1
