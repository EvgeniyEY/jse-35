<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_prePage.jsp"/>

<p class="font" style="margin: 0px">TASK UPDATE</p>

<form:form method="post" action="/tasks/update/${task.id}" modelAttribute="task">
    <div style="margin-top: 5px"> Enter new task name:</div>
    <input type="text" name="name" value=<c:out value="${task.name}"/>><br/>
    <div style="margin-top: 5px">Enter new task description:</div>
    <input type="text" name="description" value=<c:out value="${task.description}"/>><br/>
    <input class="button" style="margin-top: 10px" type="submit" value="Update task"/>
</form:form>

<jsp:include page="../resources/_postPage.jsp"/>