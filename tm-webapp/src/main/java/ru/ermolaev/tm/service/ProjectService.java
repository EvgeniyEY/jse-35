package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.exception.EmptyValueException;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    @Autowired
    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
        this.projectRepository = repository;
    }

    @NotNull
    public Project create(
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyValueException();
        if (description == null || description.isEmpty()) throw new EmptyValueException();
        @NotNull final Project project = new Project(name, description);
        return projectRepository.add(project);
    }

    @Nullable
    public Project updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyValueException();
        if (name == null || name.isEmpty()) throw new EmptyValueException();
        if (description == null || description.isEmpty()) throw new EmptyValueException();
        @Nullable Project project = projectRepository.findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    public Project findByName(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyValueException();
        return projectRepository.findByName(name);
    }

    @Nullable
    public Project removeByName(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyValueException();
        return projectRepository.removeByName(name);
    }

}
