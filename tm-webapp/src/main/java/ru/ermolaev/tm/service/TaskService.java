package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.exception.EmptyValueException;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @Autowired
    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
        this.taskRepository = repository;
    }

    @NotNull
    public Task create(
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyValueException();
        if (description == null || description.isEmpty()) throw new EmptyValueException();
        @NotNull final Task task = new Task(name, description);
        return taskRepository.add(task);
    }

    @Nullable
    public Task updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyValueException();
        if (name == null || name.isEmpty()) throw new EmptyValueException();
        if (description == null || description.isEmpty()) throw new EmptyValueException();
        @Nullable Task task = taskRepository.findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Nullable
    public Task findByName(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyValueException();
        return taskRepository.findByName(name);
    }

    @Nullable
    public Task removeByName(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyValueException();
        return taskRepository.removeByName(name);
    }
    
}
