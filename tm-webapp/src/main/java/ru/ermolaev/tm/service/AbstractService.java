package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.IRepository;
import ru.ermolaev.tm.api.service.IService;
import ru.ermolaev.tm.entity.AbstractEntity;
import ru.ermolaev.tm.exception.EmptyValueException;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    public E add(@Nullable final E e) throws Exception {
        if (e == null) throw new EmptyValueException();
        return repository.add(e);
    }

    @Nullable
    public E remove(@Nullable final E e) throws Exception {
        if (e == null) throw new EmptyValueException();
        return repository.remove(e);
    }

    public void removeAll() {
        repository.removeAll();
    }

    @Nullable
    public E findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyValueException();
        return repository.findById(id);
    }

    @Nullable
    public E removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyValueException();
        return repository.removeById(id);
    }

}
