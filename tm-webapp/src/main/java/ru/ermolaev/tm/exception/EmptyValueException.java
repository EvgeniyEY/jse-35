package ru.ermolaev.tm.exception;

public final class EmptyValueException extends AbstractException {

    public EmptyValueException() {
        super("Error! Value is empty!");
    }

}
