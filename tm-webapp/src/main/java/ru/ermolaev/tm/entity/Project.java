package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Project extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Override
    public String toString() {
        return "Project{" +
                "id=" + getId() +
                " name=" + name +
                ", description=" + description +
                '}';
    }

}
