package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Task extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Override
    public String toString() {
        return "Task{" +
                "id=" + getId() +
                " name=" + name +
                ", description=" + description +
                '}';
    }

}
