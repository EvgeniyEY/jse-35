package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @Nullable
    E add(@Nullable E e) throws Exception;

    @Nullable
    E remove(@Nullable E e) throws Exception;

    void removeAll();

    @Nullable
    E findById(@Nullable String id) throws Exception;

    @Nullable
    E removeById(@Nullable String id) throws Exception;

}
