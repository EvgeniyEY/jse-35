package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Task;

public interface ITaskService extends IService<Task> {

    @NotNull
    Task create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Task updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Task findByName(@Nullable String name) throws Exception;

    @Nullable
    Task removeByName(@Nullable String name) throws Exception;

}
