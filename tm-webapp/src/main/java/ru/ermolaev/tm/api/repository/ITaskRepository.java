package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Task;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    Task findByName(@NotNull String name);

    @Nullable
    Task removeByName(@NotNull String name);

}
