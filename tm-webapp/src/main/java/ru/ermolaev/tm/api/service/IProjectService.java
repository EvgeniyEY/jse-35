package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Project;

public interface IProjectService extends IService<Project> {

    @NotNull
    Project create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Project updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    Project findByName(@Nullable String name) throws Exception;

    @Nullable
    Project removeByName(@Nullable String name) throws Exception;

}
