package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E add(@NotNull E e);

    @NotNull
    E remove(@NotNull E e);

    void removeAll();

    @Nullable
    E findById(@NotNull String id);

    @Nullable
    E removeById(@NotNull String id);

}
