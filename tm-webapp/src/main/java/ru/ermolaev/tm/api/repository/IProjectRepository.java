package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Project;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    Project findByName(@NotNull String name);

    @Nullable
    Project removeByName(@NotNull String name);

}
