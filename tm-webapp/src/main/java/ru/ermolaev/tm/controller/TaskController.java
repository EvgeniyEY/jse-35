package ru.ermolaev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.entity.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private final ITaskService taskService;

    @Autowired
    public TaskController(@NotNull final ITaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/show")
    public ModelAndView show() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskList");
        modelAndView.addObject("tasks", taskService.findAll());
        return modelAndView;
    }

    @GetMapping("/create")
    public String create() {
        return "/task/taskCreate";
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("task") @NotNull final Task task
    ) throws Exception {
        taskService.add(task);
        return new ModelAndView("redirect:/tasks/show");
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id
    ) throws Exception {
        taskService.removeById(id);
        return new ModelAndView("redirect:/tasks/show");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) throws Exception {
        @Nullable final Task task = taskService.findById(id);
        return new ModelAndView("/task/taskUpdate", "task", task);
    }

    @PostMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @ModelAttribute("task") @NotNull final Task task
    ) throws Exception {
        taskService.updateById(id, task.getName(), task.getDescription());
        return new ModelAndView("redirect:/tasks/show");
    }

}
