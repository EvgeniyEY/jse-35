package ru.ermolaev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.entity.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    private final IProjectService projectService;

    @Autowired
    public ProjectController(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/show")
    public ModelAndView show() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectList");
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @GetMapping("/create")
    public String create() {
        return "/project/projectCreate";
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("project") @NotNull final Project project
    ) throws Exception {
        projectService.add(project);
        return new ModelAndView("redirect:/projects/show");
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id
    ) throws Exception {
        projectService.removeById(id);
        return new ModelAndView("redirect:/projects/show");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) throws Exception {
        @Nullable final Project project = projectService.findById(id);
        return new ModelAndView("/project/projectUpdate", "project", project);
    }

    @PostMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @ModelAttribute("project") @NotNull final Project project
    ) throws Exception {
        projectService.updateById(id, project.getName(), project.getDescription());
        return new ModelAndView("redirect:/projects/show");
    }

}
