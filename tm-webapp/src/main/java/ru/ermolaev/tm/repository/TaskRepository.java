package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.entity.Task;

import javax.annotation.PostConstruct;

@Repository
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @PostConstruct
    public void init() {
        entities.add(new Task("task1", "task-one"));
        entities.add(new Task("task2", "task-two"));
        entities.add(new Task("task3", "task-three"));
        entities.add(new Task("task4", "task-four"));
        entities.add(new Task("task5", "task-five"));
    }

    @Nullable
    public Task findByName(@NotNull final String name) {
        for (@NotNull final Task task : entities) if (name.equals(task.getName())) return task;
        return null;
    }

    @Nullable
    public Task removeByName(@NotNull final String name) {
        @Nullable final Task task = findByName(name);
        entities.remove(task);
        return task;
    }

}
