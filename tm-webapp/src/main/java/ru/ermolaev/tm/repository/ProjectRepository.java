package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.entity.Project;

import javax.annotation.PostConstruct;

@Repository
public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @PostConstruct
    public void init() {
        entities.add(new Project("project1", "prj-one"));
        entities.add(new Project("project2", "prj-two"));
        entities.add(new Project("project3", "prj-three"));
        entities.add(new Project("project4", "prj-four"));
        entities.add(new Project("project5", "prj-five"));
    }

    @Nullable
    public Project findByName(@NotNull final String name) {
        for (@NotNull final Project project : entities) if (name.equals(project.getName())) return project;
        return null;
    }

    @Nullable
    public Project removeByName(@NotNull final String name) {
        @Nullable final Project project = findByName(name);
        entities.remove(project);
        return project;
    }

}
