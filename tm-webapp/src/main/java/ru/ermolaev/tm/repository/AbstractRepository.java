package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.IRepository;
import ru.ermolaev.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity>  implements IRepository<E> {

    protected List<E> entities = new ArrayList<>();

    @NotNull
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    public E add(@NotNull final E e) {
        entities.add(e);
        return e;
    }

    @NotNull
    public E remove(@NotNull final E e) {
        entities.remove(e);
        return e;
    }

    public void removeAll() {
        entities.clear();
    }

    @Nullable
    public E findById(@NotNull final String id) {
        for (@NotNull final E e : entities) if (id.equals(e.getId())) return e;
        return null;
    }

    @Nullable
    public E removeById(@NotNull final String id) {
        @Nullable final E e = findById(id);
        entities.remove(e);
        return e;
    }

}
